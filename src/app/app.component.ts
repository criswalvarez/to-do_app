import { Component, OnInit } from '@angular/core';
import { Todo } from './models/todo'
import { DeleteTodoService } from './services/delete-todo.service';
import { TodoService } from './services/todo.service';
import { v4 as uuid } from 'uuid';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'todo-app';
  inputValue:string;
  todos : Array<Todo>;

  ngOnInit(): void {
    this.todos = this.todoService.getTodos()
  }

  constructor(private deletetodoservice : DeleteTodoService, private todoService : TodoService){

  }

  addTodo(){
    let insertTodo = new Todo();
    insertTodo.id = uuid.v4()
    insertTodo.description = this.inputValue;
    insertTodo.isComplete = false;
    this.todoService.addTodo(insertTodo)
    this.inputValue = null;
  }
  deleteTodo(todos:Todo){
    this.todoService.deleteTodo(todos)
  }
}
