import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Todo } from 'src/app/models/todo';



@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  @Input('todo') todo: Todo;
  @Output('onDeletetoDo') onDeletetoDo = new EventEmitter<Todo>();
 
  constructor() { }

  ngOnInit() {
  }


  delete(todo : Todo){
    this.onDeletetoDo.next(todo)
  }
}
