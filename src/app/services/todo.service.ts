import { Injectable } from '@angular/core';
import { Todo } from '../models/todo';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private todos = new Array<Todo>()

  constructor() { }

  addTodo(todo: Todo){
    this.todos.push(todo);
  }

  getTodos(){
    return this.todos;
  }

  deleteTodo(todo : Todo){
    let posicion = this.todos.findIndex(o => o.id == todo.id)
    if(posicion >= 0){
      this.todos.splice(posicion, 1)
    }
  }

}
