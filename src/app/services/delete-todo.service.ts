import { Injectable } from '@angular/core';
import { Todo } from '../models/todo';


@Injectable({
  providedIn: 'root'
})
export class DeleteTodoService {
  toDoDone = new Array<Todo>();

  constructor() { }

  deletetodo(todo : Todo){
    let posicion = this.toDoDone.findIndex(o => o.id == todo.id)
    if(posicion >= 0){
      this.toDoDone.splice(posicion, 1)
    }
  }
}
