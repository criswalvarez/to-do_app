import { v4 as uuid } from 'uuid';

export class Todo{
    id : uuid;
    description:string;
    isComplete: boolean;
}